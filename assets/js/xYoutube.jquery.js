// JavaScript jQuery Plugin xYoutube - Alexandre Mattos
(function(){

	//Definicao do nome do plugin e chamada com opcoes
	jQuery.fn.xYoutube = function(options){

		//Seta os valores default
		var defaults = {
			displayThumb:true,
			thumbSize: 1, // 0 small // 1 medium // 2 big
			displayContainer:'#video'
		}

		var options = $.extend(defaults, options);

		//Inicializacao do plugin
		return this.each(function(){

			//Objeto
			var obj = jQuery(this);

			//Adiciona classes para controle
			obj.addClass('xYoutubeLinks');
			$(options.displayContainer).addClass('xYoutubeContainer');
			var videoContainer = $('.xYoutubeContainer');

			if ( options.displayThumb == true ){

				var min = 'http://img.youtube.com/vi/_P8klPy5DBA/default.jpg';
				var med = 'http://img.youtube.com/vi/_P8klPy5DBA/mqdefault.jpg';
				var big = 'http://img.youtube.com/vi/_P8klPy5DBA/maxresdefault.jpg';

				obj.find("> li").each(function(){

					var thumbUrl = $(this).find('a');
					var partial = thumbUrl.attr('href').split("=");
					var yUrl = partial[ partial.length -1 ];

					// Verifica o tamanho do thumb
					if ( options.thumbSize == 2 ){
						thumbUrl.html('<img src="http://img.youtube.com/vi/' + yUrl + '/maxresdefault.jpg" />');
					}else if ( options.thumbSize == 1 ){
						thumbUrl.html('<img src="http://img.youtube.com/vi/' + yUrl + '/mqdefault.jpg" />');
					}else if ( options.thumbSize == 0 ){
						thumbUrl.html('<img src="http://img.youtube.com/vi/' + yUrl + '/default.jpg" />');
					}

					//Altera a url
					thumbUrl.attr("href","javascript:void(0);").attr("data-url", yUrl );

				});

			}else{
				obj.find("> li").each(function(){
					var thumbUrl = $(this).find('a');
					var partial = thumbUrl.attr('href').split("=");
					var yUrl = partial[ partial.length -1 ];
					thumbUrl.attr("href","javascript:void(0);").attr("data-url", yUrl );
				});
			}

			var btn = obj.find("> li > a");
			btn.click(function(){
				var link = $(this).attr("data-url");
				var video = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+ link +'" frameborder="0" allowfullscreen></iframe>';
				videoContainer.html(video);
			});

			btn.parent().eq(0).find('a').click();

		}); //Fim do processo

	}

})(jQuery);